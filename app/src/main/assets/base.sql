﻿-- --------------------------------------------------------

--
-- Structure de la table `Produits`
--

CREATE TABLE IF NOT EXISTS `Produits` (
  `id` integer primary key autoincrement,
  `nom` varchar(255) NOT NULL,
  `qté` integer  NOT NULL,
  `prix` float NOT NULL,
  `uri` varchar(255) NOT NULL,
  `format` varchar(64) NOT NULL,
  `code_barre` varchar(255) NOT NULL
);

-- --------------------------------------------------------

--
-- Structure de la table `Vente`
--

CREATE TABLE IF NOT EXISTS `Vente` (
  `id` integer primary key autoincrement,
  `date` date NOT NULL
);

-- --------------------------------------------------------

--
-- Structure de la table `produit_vendue`
--

CREATE TABLE IF NOT EXISTS `Produit_vendue` (
  `id_produit` integer NOT NULL,
  `id_vente` integer NOT NULL,
  `qté` integer NOT NULL,
  `prix` integer NOT NULL,
  PRIMARY KEY (`id_produit`,`id_vente`)
);
