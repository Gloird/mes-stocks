package com.gloird.messtocks.DAO;

/**
 * Created by nicolas on 30/05/2015.
 */
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.gloird.messtocks.MODEL.Vente;

import java.util.ArrayList;

public class VenteDAO extends DAO<Vente> {
    private static final String TABLE_VENTE = "vente";
    private MySQLiteHelper bdd;
    private SQLiteDatabase db;

    public VenteDAO(Context paramContext) {
        this.bdd = new MySQLiteHelper(paramContext);
    }

    public void close() {
        this.bdd.close();
    }

    public Vente create(Vente paramEvent) {
        ContentValues localContentValues = new ContentValues();
        localContentValues.put("Date", paramEvent.getDate().toString());
        this.db.insert(TABLE_VENTE, null, localContentValues);
        return null;
    }

    public Vente delete(Vente paramEvent) {
        ContentValues localContentValues = new ContentValues();
        this.db.delete(TABLE_VENTE, "id=" + paramEvent.getId(), null);
        return null;
    }

    public int getLastIdVente() {
        Cursor localCursor = this.db.query(TABLE_VENTE, null, null, null, null, null, null);
        int i = localCursor.getCount();
        localCursor.close();
        return i;
    }

    public boolean idIsExist(int paramDouble) {
        Cursor localCursor = this.db.query(TABLE_VENTE, null, "id=" + paramDouble, null, null, null, null);
        int i = localCursor.getCount();
        localCursor.close();
        return i != 0;
    }

    public void open() {
        this.db = this.bdd.getWritableDatabase();
    }

    public ArrayList<Vente> read() {
        ArrayList localArrayList = new ArrayList();
        Cursor localCursor = this.db.query(TABLE_VENTE, null, null, null, null, null, null);
        localCursor.moveToFirst();
        while (!localCursor.isAfterLast()) {
            ArrayList localArrayList2 = new ArrayList();
            localArrayList.add(new Vente(localCursor.getInt(0), localCursor.getString(1)));
            localCursor.moveToNext();
        }
        localCursor.close();
        return localArrayList;
    }

    public Vente read(Double paramDouble) {
        Cursor localCursor = this.db.query(TABLE_VENTE, null, "id=" + paramDouble, null, null, null, null);
        localCursor.moveToFirst();
        if (!localCursor.isAfterLast()) ;
        for (Vente localEvent = new Vente(localCursor.getInt(0),localCursor.getString(1)); ; localEvent = null) {
            localCursor.close();
            return localEvent;
        }
    }

    public Vente readAtCursor(int paramInt) {
        Cursor localCursor = this.db.query(TABLE_VENTE, null, null, null, null, null, null);
        localCursor.moveToFirst();
        if (localCursor.moveToPosition(paramInt)) ;
        for (Vente localEvent = new Vente(localCursor.getInt(0),localCursor.getString(1)); ; localEvent = null) {
            localCursor.close();
            return localEvent;
        }
    }

    public Vente update(Vente paramEvent) {
        ContentValues localContentValues = new ContentValues();
        localContentValues.put("id", Integer.valueOf(paramEvent.getId()));
        localContentValues.put("Date", paramEvent.getDate().toString());
        this.db.update(TABLE_VENTE, localContentValues, "id=" + paramEvent.getId(), null);
        return null;
    }
}