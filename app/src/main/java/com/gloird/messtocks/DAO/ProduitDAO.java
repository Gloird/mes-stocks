package com.gloird.messtocks.DAO;

/**
 * Created by nicolas on 30/05/2015.
 */
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.gloird.messtocks.MODEL.Produit;

import java.util.ArrayList;

public class ProduitDAO extends DAO<Produit> {
    private static final String TABLE_PRODUIT = "Produits";
    private MySQLiteHelper bdd;
    private SQLiteDatabase db;

    public ProduitDAO(Context paramContext) {
        this.bdd = new MySQLiteHelper(paramContext);
    }

    public void close() {
        this.bdd.close();
    }

    public Produit create(Produit paramEvent) {
        ContentValues localContentValues = new ContentValues();
        localContentValues.put("nom", paramEvent.getNom());
        localContentValues.put("qté", Integer.valueOf(paramEvent.getQté()));
        localContentValues.put("prix", Float.valueOf(paramEvent.getPrix()));
        localContentValues.put("URI", paramEvent.getURI());
        localContentValues.put("format", paramEvent.getFormat());
        localContentValues.put("code_barre", paramEvent.getCodeBarre());

        this.db.insert(TABLE_PRODUIT, null, localContentValues);
        return null;
    }

    public Produit delete(Produit paramEvent) {
        ContentValues localContentValues = new ContentValues();
        this.db.delete(TABLE_PRODUIT, "id=" + paramEvent.getId(), null);
        return null;
    }

    public int getLastIdEvent() {
        Cursor localCursor = this.db.query(TABLE_PRODUIT, null, null, null, null, null, null);
        int i = localCursor.getCount();
        localCursor.close();
        return i;
    }

    public boolean idIsExist(int paramint) {
        Cursor localCursor = this.db.query(TABLE_PRODUIT, null, "id=" + paramint, null, null, null, null);
        int i = localCursor.getCount();
        localCursor.close();
        return i != 0;
    }

    public void open() {
        this.db = this.bdd.getWritableDatabase();
    }

    public ArrayList<Produit> read() {
        ArrayList localArrayList = new ArrayList();
        Cursor localCursor = this.db.query(TABLE_PRODUIT, null, null, null, null, null, null);
        localCursor.moveToFirst();
        while (!localCursor.isAfterLast()) {
            localArrayList.add(new Produit(localCursor.getInt(0),localCursor.getString(1),localCursor.getInt(2),localCursor.getFloat(3),localCursor.getString(4),localCursor.getString(5),localCursor.getString(6)));
            localCursor.moveToNext();
        }
        localCursor.close();
        return localArrayList;
    }

    public Produit read(int paramint) {
        Cursor localCursor = this.db.query(TABLE_PRODUIT, null, "id=" + paramint, null, null, null, null);
        localCursor.moveToFirst();
        if (!localCursor.isAfterLast()) ;
        for (Produit localEvent = new Produit(localCursor.getInt(0),localCursor.getString(1),localCursor.getInt(2),localCursor.getFloat(3),localCursor.getString(4),localCursor.getString(5),localCursor.getString(6)); ; localEvent = null) {
            localCursor.close();
            return localEvent;
        }
    }

    public Produit readAtCursor(int paramInt) {
        Cursor localCursor = this.db.query(TABLE_PRODUIT, null, null, null, null, null, null);
        localCursor.moveToFirst();
        if (localCursor.moveToPosition(paramInt)) ;
        for (Produit localEvent = new Produit(localCursor.getInt(0),localCursor.getString(1),localCursor.getInt(2),localCursor.getFloat(3),localCursor.getString(4),localCursor.getString(5),localCursor.getString(6)); ; localEvent = null) {
            localCursor.close();
            return localEvent;
        }
    }

    public Produit update(Produit paramEvent) {
        ContentValues localContentValues = new ContentValues();
        localContentValues.put("id", Integer.valueOf(paramEvent.getId()));
        localContentValues.put("nom", paramEvent.getNom());
        localContentValues.put("qté", Integer.valueOf(paramEvent.getQté()));
        localContentValues.put("prix", Float.valueOf(paramEvent.getPrix()));
        localContentValues.put("URI", paramEvent.getURI());
        localContentValues.put("format", paramEvent.getFormat());
        localContentValues.put("code_barre", paramEvent.getCodeBarre());
        this.db.update(TABLE_PRODUIT, localContentValues, "id=" + paramEvent.getId(), null);
        return null;
    }

    public int searchProduit(String format,String codebarre){
        Cursor localCursor = this.db.query(TABLE_PRODUIT, null, "format='" + format + "' and code_barre='" + codebarre +"'", null, null, null, null);
        localCursor.moveToFirst();
        int res = -1;
        if (!localCursor.isAfterLast()){
            res = localCursor.getInt(0);
        }
        return res;
    }
}