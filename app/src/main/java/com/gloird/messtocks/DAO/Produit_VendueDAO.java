package com.gloird.messtocks.DAO;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.gloird.messtocks.MODEL.Produit;
import com.gloird.messtocks.MODEL.Produit_Vendue;

import java.util.ArrayList;

/**
 * Created by nicolas on 30/05/2015.
 */
public class Produit_VendueDAO {

    private static final String TABLE_PRODUIT_VENDUE = "Produit_vendue";

    private MySQLiteHelper bdd;
    private SQLiteDatabase db;
    private Context context;
    public Produit_VendueDAO(Context paramContext) {
        this.bdd = new MySQLiteHelper(paramContext);
        this.context = paramContext;
    }

    public void close() {
        this.bdd.close();
    }

    public Produit_Vendue create(Produit_Vendue paramPV) {
        ContentValues localContentValues = new ContentValues();
        localContentValues.put("id_vente", Integer.valueOf(paramPV.getId_vente()));
        localContentValues.put("id_produit", paramPV.getProduit().getId());
        localContentValues.put("qté", Integer.valueOf(paramPV.getQté()));
        localContentValues.put("prix", Float.valueOf(paramPV.getPrix()));
        this.db.insert(TABLE_PRODUIT_VENDUE, null, localContentValues);
        ProduitDAO DAOP = new ProduitDAO(context);
        DAOP.open();
        paramPV.getProduit().setQté(paramPV.getProduit().getQté()-paramPV.getQté());
        DAOP.update(paramPV.getProduit());
        DAOP.close();
        return null;
    }

    public Produit_Vendue delete(Produit_Vendue paramPV) {
        ContentValues localContentValues = new ContentValues();
        this.db.delete(TABLE_PRODUIT_VENDUE, "id_vente=" + paramPV.getId_vente()+" and id_produit=" + paramPV.getProduit().getId(), null);
        return null;
    }

    public void delete(int id) {
        ContentValues localContentValues = new ContentValues();
        this.db.delete(TABLE_PRODUIT_VENDUE, "id_vente=" + id, null);
    }

    public int getLastIdEvent() {
        Cursor localCursor = this.db.query(TABLE_PRODUIT_VENDUE, null, null, null, null, null, null);
        int i = localCursor.getCount();
        localCursor.close();
        return i;
    }

    public boolean idIsExist(int paramint) {
        Cursor localCursor = this.db.query(TABLE_PRODUIT_VENDUE, null, "id_vente=" + paramint, null, null, null, null);
        int i = localCursor.getCount();
        localCursor.close();
        return i != 0;
    }

    public void open() {
        this.db = this.bdd.getWritableDatabase();
    }

    public ArrayList<Produit_Vendue> read() {
        ArrayList localArrayList = new ArrayList();
        Cursor localCursor = this.db.query(TABLE_PRODUIT_VENDUE, null, null, null, null, null, null);
        localCursor.moveToFirst();
        while (!localCursor.isAfterLast()) {
            ArrayList localArrayList2 = new ArrayList();
            Produit item;
            ProduitDAO DAOP =  new ProduitDAO(context);
            DAOP.open();
            item = DAOP.read(localCursor.getInt(3));
            DAOP.close();
            localArrayList.add(new Produit_Vendue(localCursor.getInt(0), localCursor.getInt(1), localCursor.getFloat(2), item));
            localCursor.moveToNext();
        }
        localCursor.close();
        return localArrayList;
    }

    public Produit_Vendue read(int paramint) {
        Cursor localCursor = this.db.query(TABLE_PRODUIT_VENDUE, null, "id=" + paramint, null, null, null, null);
        localCursor.moveToFirst();
        if (!localCursor.isAfterLast()) ;
        Produit item;
        ProduitDAO DAOP =  new ProduitDAO(context);
        DAOP.open();
        item = DAOP.read(localCursor.getInt(3));
        DAOP.close();
        for (Produit_Vendue localEvent = new Produit_Vendue(localCursor.getInt(0), localCursor.getInt(1), localCursor.getFloat(2), item); ; localEvent = null) {
            localCursor.close();
            return localEvent;
        }
    }

    public Produit_Vendue readAtCursor(int paramInt) {
        Cursor localCursor = this.db.query(TABLE_PRODUIT_VENDUE, null, null, null, null, null, null);
        localCursor.moveToFirst();
        if (localCursor.moveToPosition(paramInt)) ;
        Produit item;
        ProduitDAO DAOP =  new ProduitDAO(context);
        DAOP.open();
        item = DAOP.read(localCursor.getInt(3));
        for (Produit_Vendue localEvent = new Produit_Vendue(localCursor.getInt(0), localCursor.getInt(1), localCursor.getFloat(2), item); ; localEvent = null) {
            localCursor.close();
            return localEvent;
        }
    }

    public Produit_Vendue update(Produit_Vendue paramPV) {
        ContentValues localContentValues = new ContentValues();
        localContentValues.put("id_vente", Integer.valueOf(paramPV.getId_vente()));
        localContentValues.put("id_produit", Integer.valueOf(paramPV.getProduit().getId()));
        localContentValues.put("qté", Integer.valueOf(paramPV.getQté()));
        localContentValues.put("prix", Float.valueOf(paramPV.getPrix()));
        this.db.update(TABLE_PRODUIT_VENDUE, localContentValues, "id_vente=" + paramPV.getId_vente()+"id_produit=" + paramPV.getProduit().getId(), null);
        return null;
    }
}

