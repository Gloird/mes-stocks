package com.gloird.messtocks.DAO;

import android.content.Context;
import android.content.res.AssetManager;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class MySQLiteHelper extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "stocks";
    private static final int DATABASE_VERSION = 1;
    private Context context;

    public MySQLiteHelper(Context paramContext) {
        super(paramContext, "stocks", null, 1);
        this.context = paramContext;
    }

    private void executeSQLScript(SQLiteDatabase database, String dbname) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        byte buf[] = new byte[1024];
        int len;
        AssetManager assetManager = context.getAssets();
        InputStream inputStream = null;

        try{
            inputStream = assetManager.open(dbname);


            while ((len = inputStream.read(buf)) != -1) {
                outputStream.write(buf, 0, len);
            }
            outputStream.close();
            inputStream.close();

            String[] createScript = outputStream.toString().split(";");


            for (int i = 0; i < createScript.length; i++) {
                String sqlStatement = createScript[i].trim();

                // TODO You may want to parse out comments here
                if (sqlStatement.length() > 0) {
                    database.execSQL(sqlStatement + ";");
                }
            }
        } catch (IOException e){
            Log.v("SQL", e.toString());
        } catch (SQLException e) {
            Log.v("SQL", e.toString());
        }
    }

    public void onCreate(SQLiteDatabase paramSQLiteDatabase) {
        try {
            Log.v("SQLite", "Creation de la table");
            executeSQLScript(paramSQLiteDatabase, "base.sql");
            Log.v("SQLite", "Creation de la table fini ");
            return;
        } catch (Exception localException) {
            Log.v("SQLite error", localException.toString());
        }
    }

    public void onUpgrade(SQLiteDatabase paramSQLiteDatabase, int paramInt1, int paramInt2) {
        paramSQLiteDatabase.execSQL("DROP TABLE IF EXISTS Produits");
        paramSQLiteDatabase.execSQL("DROP TABLE IF EXISTS Vente");
        paramSQLiteDatabase.execSQL("DROP TABLE IF EXISTS Produit_vendue");
        onCreate(paramSQLiteDatabase);
    }
}