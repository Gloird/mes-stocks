package com.gloird.messtocks.DAO;

/**
 * Created by nicolas on 30/05/2015.
 */
public abstract class DAO<T> {
    public abstract T create(T paramT);

    public abstract T delete(T paramT);

    public abstract T update(T paramT);
}