package com.gloird.messtocks.IHM;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.desmond.squarecamera.ImageUtility;
import com.gloird.messtocks.DAO.ProduitDAO;
import com.gloird.messtocks.DAO.Produit_VendueDAO;
import com.gloird.messtocks.DAO.VenteDAO;
import com.gloird.messtocks.MODEL.Produit;
import com.gloird.messtocks.MODEL.Produit_Vendue;
import com.gloird.messtocks.MODEL.Vente;
import com.gloird.messtocks.R;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CaisseActivity extends Activity implements View.OnClickListener {

    private List<Produit_Vendue> listProduits;
    private ListView lv_produits;
    private Button btn_scan,btn_reset,btn_fini;
    private TextView tv_prixtotal;
    private Context context;
    private ProduitDAO DAOP;
    private VenteDAO DAOV;
    private Produit_VendueDAO DAOPV;
    private TextView tv_qte;
    private Produit P;
    private Produit_Vendue PV;
    private float prixT = 0;
    private int idVente;
    private Button btn_plus,btn_moins;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_caisse);
        setTitle("Caisse");
        ActionBar actionBar = this.getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        lv_produits = (ListView)findViewById(R.id.lv_produits);
        btn_scan = (Button)findViewById(R.id.btn_scan);
        btn_reset = (Button)findViewById(R.id.btn_reset);
        btn_fini = (Button)findViewById(R.id.btn_fini);
        tv_prixtotal = (TextView)findViewById(R.id.et_prixtotal);
        context = this;

        btn_fini.setOnClickListener(this);
        btn_reset.setOnClickListener(this);
        btn_scan.setOnClickListener(this);

        DAOP = new ProduitDAO(this);
        DAOP.open();

        DAOV = new VenteDAO(this);
        DAOV.open();
        Date d = new Date();
        DAOV.create(new Vente(d));
        idVente = DAOV.getLastIdVente();

        DAOPV = new Produit_VendueDAO(this);
        DAOPV.open();

        listProduits = new ArrayList<Produit_Vendue>();
    }

    @Override
    public void onClick(View v) {
        if(v.getId()==R.id.btn_scan) {
            IntentIntegrator scanIntegrator = new IntentIntegrator(this);
            scanIntegrator.initiateScan();
        }
        if(v.getId()==R.id.btn_reset) {
            DAOPV.delete(idVente);
            DAOV.delete(new Vente(idVente , ""));
            this.finish();

        }
        if(v.getId()==R.id.btn_fini) {
            this.finish();
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        //retrieve scan result
        IntentResult scanningResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
        if (scanningResult != null) {
            //we have a result
            String scanContent = scanningResult.getContents();
            String scanFormat = scanningResult.getFormatName();

            int id = DAOP.searchProduit(scanFormat,scanContent);
            P = DAOP.read(id);

            final Dialog dialog = new Dialog(this);
            dialog.setContentView(R.layout.dialog_produitscan);
            dialog.setTitle("Produit sacanner");
            // set the custom dialog components - text, image and button
            TextView tv_nom = (TextView) dialog.findViewById(R.id.tv_nom);
            ImageView iv_produit = (ImageView) dialog.findViewById(R.id.iv_produit);
            TextView tv_prix = (TextView) dialog.findViewById(R.id.tv_prix);
            tv_qte = (TextView) dialog.findViewById(R.id.tv_qte);
            btn_plus = (Button) dialog.findViewById(R.id.btn_plus);
            btn_moins = (Button) dialog.findViewById(R.id.btn_moins);
            Button btn_annuler = (Button) dialog.findViewById(R.id.btn_annuler);
            Button btn_continuer = (Button) dialog.findViewById(R.id.btn_continuer);
            btn_moins.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int temp = Integer.parseInt(tv_qte.getText().toString())-1;
                    if(temp < 0){ temp = 0; }
                    tv_qte.setText(temp+"");
                }
            });
            btn_plus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int temp = Integer.parseInt(tv_qte.getText().toString())+1;
                    if(temp > P.getQté()){
                        temp = P.getQté();
                    }
                    tv_qte.setText(temp + "");
                }
            });

            btn_annuler.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
            btn_continuer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    PV = new Produit_Vendue(idVente,Integer.parseInt(tv_qte.getText().toString()),P.getPrix(),P);
                    listProduits.add(PV);
                    DAOPV.create(PV);
                    refreshlist();
                    dialog.dismiss();
                }
            });
            tv_nom.setText(P.getNom());
            tv_prix.setText(Float.toString(P.getPrix()));
            Bitmap bitmap = ImageUtility.decodeSampledBitmapFromPath(P.getURI(), iv_produit.getMaxWidth(), iv_produit.getMaxHeight());
            iv_produit.setImageBitmap(bitmap);

            dialog.show();

        }else{
            Toast toast = Toast.makeText(getApplicationContext(),
                    "No scan data received!", Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    private void refreshlist(){
        ProduitsVendueAdapter customAdapter = new ProduitsVendueAdapter(this, R.layout.list_produit, listProduits);
        lv_produits.setAdapter(customAdapter);
        float p = (PV.getPrix()*PV.getQté());
        prixT = prixT+p;
        tv_prixtotal.setText(Float.toString(prixT));
    }
}
