package com.gloird.messtocks.IHM;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.desmond.squarecamera.CameraActivity;
import com.desmond.squarecamera.ImageUtility;
import com.gloird.messtocks.DAO.ProduitDAO;
import com.gloird.messtocks.MODEL.Produit;
import com.gloird.messtocks.R;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

public class ModifActivity extends Activity {

    private static final int REQUEST_CAMERA = 0;
    private ImageView iv_produit;
    private Button btn_modphoto, btn_mod;
    private TextView tv_prix,tv_id,tv_nom,tv_qtestock,tv_qtevendue,tv_format,tv_content;
    private Produit P;
    private Context context;
    private Menu menu;
    private String scanFormat;
    private String scanContent;
    private Point mSize;
    private String photoUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modif);
        setTitle("Produit");
        ActionBar actionBar = this.getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        //Initialisation
        context = this;
        iv_produit = (ImageView)findViewById(R.id.iv_produit);
        btn_mod = (Button)findViewById(R.id.btn_mod);
        btn_modphoto = (Button)findViewById(R.id.btn_modphoto);
        tv_prix = (TextView)findViewById(R.id.et_prixtotal);
        tv_nom = (TextView)findViewById(R.id.tv_nom);
        tv_id = (TextView)findViewById(R.id.tv_id);
        tv_qtestock = (TextView)findViewById(R.id.et_qtestock);
        tv_qtevendue = (TextView)findViewById(R.id.tv_qtevendue);
        tv_format = (TextView)findViewById(R.id.tv_format);
        tv_content = (TextView)findViewById(R.id.tv_content);

        int position = getIntent().getIntExtra("position", -1);

        Display display = getWindowManager().getDefaultDisplay();
        mSize = new Point();
        display.getSize(mSize);

        if(position != -1){
            final ProduitDAO DAOP = new ProduitDAO(this);
            DAOP.open();
            P = DAOP.readAtCursor(position);
            tv_nom.setText(P.getNom());
            tv_prix.setText(Float.toString(P.getPrix())+"€");
            tv_id.setText(P.getId()+"");
            tv_qtevendue.setText("0");
            tv_qtestock.setText(P.getQté()+"");
            tv_format.setText(P.getFormat());
            tv_content.setText(P.getCodeBarre());
            Bitmap bitmap = ImageUtility.decodeSampledBitmapFromPath(P.getURI(), 1080 / 3, 1776 /3);
            iv_produit.setImageBitmap(bitmap);
            btn_mod.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    setContentView(R.layout.activity_modifedit);
                    final EditText etv_prix = (EditText)findViewById(R.id.et_prixtotal);
                    final EditText etv_nom = (EditText)findViewById(R.id.tv_nom);
                    tv_format = (TextView)findViewById(R.id.tv_format);
                    iv_produit = (ImageView)findViewById(R.id.iv_produit);
                    tv_content = (TextView)findViewById(R.id.tv_content);
                    final EditText etv_qtestock = (EditText)findViewById(R.id.et_qtestock);
                    photoUri = P.getURI();
                    Bitmap bitmap = ImageUtility.decodeSampledBitmapFromPath(P.getURI(), iv_produit.getMaxHeight(), iv_produit.getMaxWidth());
                    iv_produit.setImageBitmap(bitmap);
                    etv_nom.setText(P.getNom());
                    etv_prix.setText(Float.toString(P.getPrix()));
                    etv_qtestock.setText(P.getQté()+"");
                    tv_format.setText(P.getFormat());
                    tv_content.setText(P.getCodeBarre());
                    btn_mod = (Button)findViewById(R.id.btn_mod);
                    btn_modphoto = (Button)findViewById(R.id.btn_modphoto);
                    btn_modphoto.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent startCustomCameraIntent = new Intent(context, CameraActivity.class);
                            startActivityForResult(startCustomCameraIntent, REQUEST_CAMERA);
                        }
                    });
                    btn_mod.setText("Enregistrer");
                    menu.findItem(R.id.action_mode).setTitle("MODE ECRITURE");
                    Button btn_scan = (Button)findViewById(R.id.btn_scan);
                    btn_scan.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            IntentIntegrator scanIntegrator = new IntentIntegrator((Activity) context);
                            scanIntegrator.initiateScan();
                        }
                    });

                    btn_mod.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            P.setNom(etv_nom.getText().toString());
                            P.setPrix(Float.parseFloat(etv_prix.getText().toString()));
                            P.setQté(Integer.parseInt(etv_qtestock.getText().toString()));
                            P.setNom(etv_nom.getText().toString());
                            P.setCodeBarre(tv_format.getText().toString(), tv_content.getText().toString());
                            iv_produit.getMaxHeight();
                            P.setURI(photoUri);
                            DAOP.update(P);
                            finishActivity();
                        }
                    });
                }
            });
        }
    }

    private void finishActivity(){
        this.finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        this.menu = menu;
        getMenuInflater().inflate(R.menu.menu_modif, menu);
        return true;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        //retrieve scan result
        IntentResult scanningResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
        if (resultCode != RESULT_OK) return;
        if (requestCode == REQUEST_CAMERA) {
            photoUri = intent.getData().getPath();
            Log.w("test", mSize.toString());
            Bitmap bitmap = ImageUtility.decodeSampledBitmapFromPath(photoUri, iv_produit.getMaxHeight(), iv_produit.getMaxWidth());
            iv_produit.setImageBitmap(bitmap);
        } else {
            if (scanningResult != null) {
                //we have a result
                scanContent = scanningResult.getContents();
                scanFormat = scanningResult.getFormatName();

                tv_format.setText(scanFormat);
                tv_content.setText(scanContent);
            } else {
                Toast toast = Toast.makeText(getApplicationContext(),
                        "No scan data received!", Toast.LENGTH_SHORT);
                toast.show();
            }

        }
    }
}
