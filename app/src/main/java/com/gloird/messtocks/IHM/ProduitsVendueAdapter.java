package com.gloird.messtocks.IHM;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.desmond.squarecamera.ImageUtility;
import com.gloird.messtocks.MODEL.Produit;
import com.gloird.messtocks.MODEL.Produit_Vendue;
import com.gloird.messtocks.R;

import java.util.List;

/**
 * Created by nicolas on 31/05/2015.
 */
public class ProduitsVendueAdapter extends ArrayAdapter<Produit_Vendue> {

    private Context context;

    public ProduitsVendueAdapter(Context context, int textViewResourceId) {
        super(context, textViewResourceId);
        this.context = context;
    }

    public ProduitsVendueAdapter(Context context, int resource, List<Produit_Vendue> items) {
        super(context, resource, items);
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;

        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.list_produit, null);
        }

        Produit_Vendue p = getItem(position);

        if (p != null) {
            TextView tv_nom = (TextView) v.findViewById(R.id.tv_nom);
            TextView tv_qté = (TextView) v.findViewById(R.id.tv_qté);
            TextView tv_prix = (TextView) v.findViewById(R.id.tv_prix);
            ImageView iv_produit =(ImageView) v.findViewById(R.id.iv_produit);
            if (tv_nom != null) {
                tv_nom.setText(p.getProduit().getNom());
            }

            if (tv_qté != null) {
                tv_qté.setText(Integer.toString(p.getQté()));
            }

            if (tv_prix != null) {
                Float f = (p.getPrix()*p.getQté());
                tv_prix.setText(f+"€");
            }

            if (iv_produit != null) {
                Bitmap bitmap = ImageUtility.decodeSampledBitmapFromPath(p.getProduit().getURI(), iv_produit.getMaxWidth(), iv_produit.getMaxWidth());
                iv_produit.setImageBitmap(bitmap);
            }
        }

        return v;
    }

}