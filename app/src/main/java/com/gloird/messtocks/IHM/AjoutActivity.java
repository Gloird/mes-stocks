package com.gloird.messtocks.IHM;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.desmond.squarecamera.CameraActivity;
import com.desmond.squarecamera.ImageUtility;
import com.gloird.messtocks.DAO.ProduitDAO;
import com.gloird.messtocks.MODEL.Produit;
import com.gloird.messtocks.R;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

public class AjoutActivity extends Activity implements View.OnClickListener {

    private static final int REQUEST_CAMERA = 0;
    private ImageView iv_produit;
    private Button btn_modphoto, btn_mod,btn_scan;
    private TextView tv_format,tv_content;
    private EditText et_prix,et_nom,et_qtestock;
    private Produit P;
    private Context context;
    private String scanFormat = "";
    private String scanContent = "";
    private Point mSize;
    private String photoUri = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ajout);
        setTitle("Ajout Produit");
        ActionBar actionBar = this.getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        //Initialisation
        context = this;
        iv_produit = (ImageView)findViewById(R.id.iv_produit);
        btn_mod = (Button)findViewById(R.id.btn_mod);
        btn_modphoto = (Button)findViewById(R.id.btn_modphoto);
        btn_scan = (Button)findViewById(R.id.btn_scan);
        et_prix = (EditText)findViewById(R.id.et_prixtotal);
        et_nom = (EditText)findViewById(R.id.tv_nom);
        et_qtestock = (EditText)findViewById(R.id.et_qtestock);
        tv_format = (TextView)findViewById(R.id.tv_format);
        tv_content = (TextView)findViewById(R.id.tv_content);

        btn_scan.setOnClickListener(this);
        btn_mod.setOnClickListener(this);
        btn_modphoto.setOnClickListener(this);

        Display display = getWindowManager().getDefaultDisplay();
        mSize = new Point();
        display.getSize(mSize);
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.btn_modphoto){
            Intent startCustomCameraIntent = new Intent(this, CameraActivity.class);
            startActivityForResult(startCustomCameraIntent, REQUEST_CAMERA);
        }
        if(v.getId() == R.id.btn_scan){
            IntentIntegrator scanIntegrator = new IntentIntegrator((Activity) context);
            scanIntegrator.initiateScan();
        }
        if(v.getId() == R.id.btn_mod){
            P = new Produit();
            P.setCodeBarre(scanFormat,scanContent);
            P.setQté(Integer.parseInt(et_qtestock.getText().toString()));
            P.setPrix(Float.parseFloat(et_prix.getText().toString()));
            P.setNom(et_nom.getText().toString());
            P.setURI(photoUri);

            ProduitDAO DAOP = new ProduitDAO(this);
            DAOP.open();
            DAOP.create(P);
            DAOP.close();
            this.finish();
        }
    }
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {

        IntentResult scanningResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
        if (resultCode != RESULT_OK) return;
        if (requestCode == REQUEST_CAMERA) {
            photoUri = intent.getData().getPath();
            Bitmap bitmap = ImageUtility.decodeSampledBitmapFromPath(photoUri, iv_produit.getMaxHeight(), iv_produit.getMaxWidth());
            iv_produit.setImageBitmap(bitmap);
        }else {
            if (scanningResult != null) {
                scanContent = scanningResult.getContents();
                scanFormat = scanningResult.getFormatName();

                tv_format.setText(scanFormat);
                tv_content.setText(scanContent);

            } else {
                Toast toast = Toast.makeText(getApplicationContext(),
                        "No scan data received!", Toast.LENGTH_SHORT);
                toast.show();
            }
        }
        super.onActivityResult(requestCode, resultCode, intent);
    }
}
