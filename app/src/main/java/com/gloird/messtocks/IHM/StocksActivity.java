package com.gloird.messtocks.IHM;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.gloird.messtocks.DAO.ProduitDAO;
import com.gloird.messtocks.MODEL.Produit;
import com.gloird.messtocks.R;

import java.util.List;

public class StocksActivity extends Activity {

    private ListView lv_produits;
    private ProduitDAO DAOP;
    private List<Produit> listProduits;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stocks);
        setTitle("Stocks");
        ActionBar actionBar = this.getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        lv_produits = (ListView)findViewById(R.id.lv_produits);

        DAOP = new ProduitDAO(this);
        DAOP.open();
        listProduits = DAOP.read();

        ProduitsAdapter customAdapter = new ProduitsAdapter(this, R.layout.list_produit, listProduits);

        lv_produits.setAdapter(customAdapter);

        lv_produits.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent myIntent = new Intent(StocksActivity.this, ModifActivity.class);
                myIntent.putExtra("position",position);
                startActivity(myIntent);
            }
        });

    }

    @Override
    protected void onStop() {
        super.onStop();
        DAOP.close();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        DAOP.close();
    }

    @Override
    protected void onResume() {
        super.onResume();
        DAOP = new ProduitDAO(this);
        DAOP.open();
        listProduits = DAOP.read();

        ProduitsAdapter customAdapter = new ProduitsAdapter(this, R.layout.list_produit, listProduits);

        lv_produits.setAdapter(customAdapter);

        lv_produits.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent myIntent = new Intent(StocksActivity.this, ModifActivity.class);
                myIntent.putExtra("position",position);
                startActivity(myIntent);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_stocks, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        switch (item.getItemId()) {
            case R.id.action_search:
                //rechercher un produit
                return true;
            case R.id.action_ajout:
                //ajouter un produit
                Intent myIntent = new Intent(StocksActivity.this, AjoutActivity.class);
                startActivity(myIntent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
