package com.gloird.messtocks.IHM;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.desmond.squarecamera.ImageUtility;
import com.gloird.messtocks.MODEL.Produit;
import com.gloird.messtocks.R;

import java.util.List;

/**
 * Created by nicolas on 31/05/2015.
 */
public class ProduitsAdapter extends ArrayAdapter<Produit> {

    private Context context;

    public ProduitsAdapter(Context context, int textViewResourceId) {
        super(context, textViewResourceId);
        this.context = context;
    }

    public ProduitsAdapter(Context context, int resource, List<Produit> items) {
        super(context, resource, items);
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;

        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.list_produit, null);
        }

        Produit p = getItem(position);

        if (p != null) {
            TextView tv_nom = (TextView) v.findViewById(R.id.tv_nom);
            TextView tv_qté = (TextView) v.findViewById(R.id.tv_qté);
            TextView tv_prix = (TextView) v.findViewById(R.id.tv_prix);
            ImageView iv_produit =(ImageView) v.findViewById(R.id.iv_produit);
            if (tv_nom != null) {
                tv_nom.setText(p.getNom());
            }

            if (tv_qté != null) {
                tv_qté.setText(Integer.toString(p.getQté()));
            }

            if (tv_prix != null) {
                tv_prix.setText(Float.toString(p.getPrix())+"€");
            }

            if (iv_produit != null) {
                Bitmap bitmap = ImageUtility.decodeSampledBitmapFromPath(p.getURI(), 1080 / 3, 1776 / 3);
                iv_produit.setImageBitmap(bitmap);
            }
        }

        return v;
    }

}