package com.gloird.messtocks.MODEL;

/**
 * Created by nicolas on 30/05/2015.
 */
public class Produit_Vendue {

    private int id_vente;
    private int qté;
    private float prix;
    private Produit Produit;

    public Produit_Vendue(int id_vente, int qté, float prix, Produit Produit) {
        this.id_vente = id_vente;
        this.qté = qté;
        this.prix = prix;
        this.Produit = Produit;
    }

    public int getId_vente() {
        return id_vente;
    }

    public void setId_vente(int id_vente) {
        this.id_vente = id_vente;
    }

    public int getQté() {
        return qté;
    }

    public void setQté(int qté) {
        this.qté = qté;
    }

    public float getPrix() {
        return prix;
    }

    public void setPrix(float prix) {
        this.prix = prix;
    }

    public Produit getProduit() {
        return Produit;
    }

    public void setProduit(Produit Produit) {
        this.Produit = Produit;
    }

    @Override
    public String toString() {
        return "produit_vendue{" +
                "id_vente=" + id_vente +
                ", qté=" + qté +
                ", prix=" + prix +
                ", produit=" + Produit +
                '}';
    }
}
