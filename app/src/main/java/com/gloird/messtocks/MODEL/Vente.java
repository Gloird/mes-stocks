package com.gloird.messtocks.MODEL;

import java.util.Date;
import java.util.List;

/**
 * Created by nicolas on 30/05/2015.
 */
public class Vente {

    private int id;
    private String date;

    public Vente(int id, String date) {
        this.id = id;
        this.date = date;
    }

    public Vente(Date d){
        this.date = d.getDay()+"-"+d.getMonth()+"-"+d.getYear()+" "+d.getHours()+":"+d.getMinutes()+":"+d.getSeconds();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "vente{" +
                "id=" + id +
                ", date=" + date +
                '}';
    }
}
