package com.gloird.messtocks.MODEL;

/**
 * Created by nicolas on 30/05/2015.
 */


public class Produit {
    private int id;
    private String nom;
    private int qté;
    private float prix;
    private String format;
    private String codeBarre;
    private String URI;

    public Produit(int id, String nom, int qté, float prix, String URI, String format, String codeBarre) {
        this.id = id;
        this.nom = nom;
        this.qté = qté;
        this.prix = prix;
        this.format = format;
        this.codeBarre = codeBarre;
        this.URI = URI;
    }

    public Produit() {

    }

    public String getURI() {
        return URI;
    }

    public void setURI(String URI) {
        this.URI = URI;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getQté() {
        return qté;
    }

    public void setQté(int qté) {
        this.qté = qté;
    }

    public float getPrix() {
        return prix;
    }

    public void setPrix(float prix) {
        this.prix = prix;
    }

    public String getFormat() {
        return format;
    }

    public String getCodeBarre() {
        return codeBarre;
    }

    public void setCodeBarre(String format,String codeBarre) {
        this.codeBarre = codeBarre;
        this.format = format;
    }

    @Override
    public String toString() {
        return "Produit{" +
                "id=" + id +
                ", nom='" + nom + '\'' +
                ", qté=" + qté +
                ", prix=" + prix +
                ", format='" + format + '\'' +
                ", codeBarre='" + codeBarre + '\'' +
                ", URI='" + URI + '\'' +
                '}';
    }
}
